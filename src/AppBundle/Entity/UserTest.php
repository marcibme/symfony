<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="UserTest")
 */
class UserTest
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    public $name;

    /**
     * @ORM\Column(type="smallint")
     */
    public $gender;

    /**
     * @ORM\Column(type="string", length=100)
     */
    public $address;

    /**
     * @ORM\Column(type="string", length=255)
     */
    public $aboutMe;

    /**
     * @ORM\Column(type="smallint")
     */
    public $isActive;
}

//, isActive(smallint)]