<?php

namespace AppBundle\Form;

use AppBundle\Entity\UserTest;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class UserTestFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, array('label' => 'név'))
            ->add('gender', ChoiceType::class, array(
                'choices' => array(
                    '0' => 'férfi',
                    '1' => 'nő',
                ),
                'expanded' => true
                ,'label' => 'nem'))
            ->add('address', TextType::class, array('label' => 'cím'))
            ->add('aboutMe', TextareaType::class, array('label' => 'rólam'))
            ->add('isActive', CheckboxType::class, array('label' => 'aktív-e'))
            ->add('save', SubmitType::class, array('label' => 'Mentés'))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => UserTest::class,
        ));
    }
}