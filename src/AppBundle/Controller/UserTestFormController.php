<?php
/**
 * Created by PhpStorm.
 * User: kishonthym
 * Date: 2017. 06. 16.
 * Time: 22:44
 */

namespace AppBundle\Controller;



//use AppBundle\Form\UserType;
//use AppBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;

use AppBundle\Entity\UserTest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Form\UserTestFormType;

class UserTestFormController extends Controller
{
    /**
     * @Route("/user_test_form")
     */
    function showAction(Request $request)
    {

        $usertest = new UserTest();
        $form = $this->createForm(UserTestFormType::class, $usertest);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            // 3) Encode the password (you could also do this via Doctrine listener)


            // 4) save the User!
            $em = $this->getDoctrine()->getManager();
            $em->persist($usertest);
            $em->flush();

            // ... do any other work - like sending them an email, etc
            // maybe set a "flash" success message for the user

            //return $this->redirectToRoute('show.html.twig');
            return $this->render('user_test_form/show.html.twig', array(
                'name' => "dasd"
            ));
        }

        return $this->render(
            'user_test_form/UserTestForm.html.twig',
            array('form' => $form->createView())
        );
    }
}